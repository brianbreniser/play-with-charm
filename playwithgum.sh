# If no arguments, use gum to choose between 1 and 5

if [ $# -eq 0 ]; then
    choose=$(gum choose 1 2 3 4 5)
else
    choose=$1
fi

echo "You chose: $choose"

fav=$(gum input --placeholder "What's your favorite number")

echo "Your favorite number is: $fav"

long=$(gum write --placeholder "Write a long sentence, (ctrl+d to finish)")

echo "You wrote: $long"

yes(){
    gum style --foreground 202 --border-foreground "#FF69B4" --border double --align center --width 50 --margin "1 2" --padding "2 4" 'I hope that was a good idea' 'no going back now'
}

no(){
    gum style --foreground 202 --border-foreground 212 --border double --align center --width 50 --margin "1 2" --padding "2 4" 'Pfew, that was close'
}

gum confirm "Commit changes?" && gum spin -s line --title "Welp no going back now" -- sleep 5 && yes || no

pass=$(gum input --password --placeholder "Enter your password")

echo "hope that wans't sensitive: $pass"

echo "Pick a card, any card..."
CARD=$(gum choose --height 12 {{A,K,Q,J},{10..2}}" "{♠,♥,♣,♦})
echo "Was your card the $CARD?"


