package main

import (
	"fmt"

	"github.com/charmbracelet/huh"
	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/log"
)

/*
	Reminders
	Log levels
		log.Error("1")
		log.Warn("2")
		log.Info("3")
		log.Debug("4")

		// All shows fatal of course
		log.Fatal("All levels get this")
*/

const (
	black          = lipgloss.Color("#000000")
	white          = lipgloss.Color("#FFFFFF")
	notquiteblack  = lipgloss.Color("#111111")
	blue           = lipgloss.Color("#0000FF")
	green          = lipgloss.Color("#00FF00")
	red            = lipgloss.Color("#FF0000")
	yellow         = lipgloss.Color("#FFFF00")
	orange         = lipgloss.Color("#FFA500")
	pink           = lipgloss.Color("#FF00FF")
	cyan           = lipgloss.Color("#00FFFF")
	darkblue       = lipgloss.Color("#0000A0")
	darkgreen      = lipgloss.Color("#008000")
	darkred        = lipgloss.Color("#800000")
	darkyellow     = lipgloss.Color("#808000")
	darkpink       = lipgloss.Color("#800080")
	darkcyan       = lipgloss.Color("#008080")
	gray           = lipgloss.Color("#808080")
	darkgray       = lipgloss.Color("#404040")
	lightgray      = lipgloss.Color("#C0C0C0")
	lipglosspink   = lipgloss.Color("#FF69B4") // Also 212
	lipglossorange = lipgloss.Color("202")
	lipglosspurple = lipgloss.Color("#800080")
)

func minimalgreen() lipgloss.Style {
	return lipgloss.NewStyle().
		Foreground(green).
		Border(lipgloss.NormalBorder()).
		BorderForeground(green)
}

var (
	burger       string
	toppings     []string
	sauceLevel   int
	name         string
	instructions string
	discount     bool
	country      string
)

func displayResults() {
	fmt.Println(minimalgreen().Render("Burger: " + burger))
	fmt.Println(minimalgreen().Render("Toppings: " + fmt.Sprintf("%v", toppings)))
	fmt.Println(minimalgreen().Render("Sauce level: " + fmt.Sprintf("%d", sauceLevel)))
	fmt.Println(minimalgreen().Render("Name: " + name))
	fmt.Println(minimalgreen().Render("Instructions: " + instructions))
	fmt.Println(minimalgreen().Render("Discount: " + fmt.Sprintf("%t", discount)))
	fmt.Println(minimalgreen().Render("Country: " + country))
}

func graybox() lipgloss.Style {
	return lipgloss.NewStyle().
		Foreground(white).
		Background(notquiteblack).
		Padding(1)
}

func redbox() lipgloss.Style {
	return lipgloss.NewStyle().
		Foreground(lipglossorange).
		Border(lipgloss.NormalBorder()).
		BorderForeground(red).
		Padding(1)
}

func greenbox() lipgloss.Style {
	return lipgloss.NewStyle().
		Foreground(green).
		Border(lipgloss.DoubleBorder()).
		BorderForeground(green).
		Padding(1)
}

func main() {
	log.SetLevel(log.DebugLevel)
	log.Info("Starting the program")

	fmt.Println(graybox().Render("testing graybox"))
	fmt.Println(redbox().Render("testing redbox"))
	fmt.Println(greenbox().Render("testing greenbox"))

	// Create a new graygray
	graygray := lipgloss.NewStyle().
		Foreground(white).
		Background(darkgray).
		Width(30).
		Align(lipgloss.Center)

	fmt.Println(graygray.Render("Hello, World!"))
	fmt.Println(graygray.Render("Twice!"))

	boxy := lipgloss.NewStyle().
		Foreground(lipglossorange).
		Padding(1).
		Margin(1).
		Border(lipgloss.DoubleBorder()).
		BorderForeground(lipglosspink).
		Width(30).
		Align(lipgloss.Center)

	fmt.Println(boxy.Render("this has gotta be way tooo long of a line and i wanna see what it does"))

	grayboxy := lipgloss.NewStyle().
		Bold(true).
		Foreground(lipglosspink).
		Background(notquiteblack).
		Padding(1).
		Margin(2)

	fmt.Println(grayboxy.Render("this is a test"))

	log.Info("Starting the questions")
	form := huh.NewForm(
		huh.NewGroup(
			huh.NewSelect[string]().
				Title("Choose your burger").
				Options(
					huh.NewOption("Charmburger Classic", "classic"),
					huh.NewOption("Chickwich", "chickwich"),
					huh.NewOption("Fishburger", "fishburger"),
					huh.NewOption("Charmpossible™ Burger", "charmpossible"),
				).
				Value(&burger),
			huh.NewMultiSelect[string]().
				Title("Toppings").
				Options(
					huh.NewOption("Lettuce", "lettuce").Selected(true),
					huh.NewOption("Tomato", "tomato").Selected(true),
					huh.NewOption("Onion", "onion"),
					huh.NewOption("Pickles", "pickles"),
					huh.NewOption("Cheese", "cheese"),
				).
				Limit(4).
				Value(&toppings),

			huh.NewSelect[int]().
				Title("How much charm sauce?").
				Options(
					huh.NewOption("None", 0),
					huh.NewOption("A little", 1),
					huh.NewOption("A lot", 2),
				).
				Value(&sauceLevel),
		),

		huh.NewGroup(
			huh.NewInput().
				Title("What is your name").
				Value(&name).
				Validate(func(str string) error {
					if str == "" {
						return fmt.Errorf("name is required")
					}
					return nil
				}),
			huh.NewText().
				Title("Special instructions").
				CharLimit(400).
				Value(&instructions),

			huh.NewConfirm().
				Title("Would you like a discount?").
				Value(&discount),

			huh.NewSelect[string]().
				Title("Pick a country.").
				Options(
					huh.NewOption("United States", "US"),
					huh.NewOption("Germany", "DE"),
					huh.NewOption("Brazil", "BR"),
					huh.NewOption("Canada", "CA"),
				).
				Value(&country),
		),
	)

	err := form.Run()
	if err != nil {
		log.Fatal(err)
	}

	if discount {
		fmt.Println(greenbox().Render("You got a discount"))
	} else {
		fmt.Println(redbox().Render("You didn't get a discount"))
	}

	displayResults()

	var oneoff string

	err = huh.NewInput().
		Title("What is the oneoff?").
		Value(&oneoff).
		Run()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(greenbox().Render("Your oneoff: " + oneoff))

	err = huh.NewNote().
		Title("This is a note field").
		Run()
	if err != nil {
		log.Fatal(err)
	}

	var instructions string

	err = huh.NewText().
		Title("This is a text field").
		CharLimit(400).
		Placeholder("Enter your instructions here alt+enter for newlines").
		Value(&instructions).
		Run()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(greenbox().Render("Your instructions: " + instructions))

	fmt.Println(greenbox().Render("End of program"))
}
