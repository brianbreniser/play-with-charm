cmd="oc new-build"

# Choose build strategy
strategy=$(gum choose --header="Choose the build strategy:" "docker" "source" "pipeline")

case $strategy in
    "pipeline")
        gum log --level warn "Pipeline strategy is deprecated. Consider using Jenkinsfiles directly on Jenkins or OpenShift Pipelines."
        ;;
    *)
        cmd="$cmd --strategy=$strategy"
        ;;
esac

# Check if binary build is needed
if gum confirm "Is this a binary build?"; then
    cmd="$cmd --binary"
fi

# Get name of the app if needed
name=$(gum input --placeholder "Enter the name of the application (optional)")
if [ -n "$name" ]; then
    cmd="$cmd --name=$name"
fi

# Source code URL or path
if [ "$strategy" != "binary" ]; then
    source=$(gum input --placeholder "Enter the URL or Path to the source code")
    if [ -n "$source" ]; then
        cmd="$cmd $source"
    fi
fi

# Context directory
context_dir=$(gum input --placeholder "Enter the context directory (optional)")
if [ -n "$context_dir" ]; then
    cmd="$cmd --context-dir=$context_dir"
fi

# Dockerfile
if [ "$strategy" = "docker" ]; then
    dockerfile=$(gum input --placeholder "Enter the path to the Dockerfile (optional)")
    if [ -n "$dockerfile" ]; then
        cmd="$cmd --dockerfile=$dockerfile"
    fi
fi

# Environment Variables
envlist=()
while true; do
    env=$(gum input --placeholder "Specify an environment variable (KEY=value, leave blank to stop)")
    if [ -z "$env" ]; then
        break
    fi
    envlist+=("$env")
done

for env in "${envlist[@]}"; do
    cmd="$cmd -e $env"
done

# Build Arguments
build_args=()
while true; do
    build_arg=$(gum input --placeholder "Specify a build argument (KEY=value, leave blank to stop)")
    if [ -z "$build_arg" ]; then
        break
    fi
    build_args+=("$build_arg")
done

for build_arg in "${build_args[@]}"; do
    cmd="$cmd --build-arg=$build_arg"
done

# Image stream
if gum confirm "Do you want to specify an image stream to use as a builder?"; then
    image_stream=$(gum input --placeholder "Enter the name of the image stream")
    cmd="$cmd -i $image_stream"
fi

# Push to image stream
if gum confirm "Push to image stream?"; then
    image_stream=$(gum input --placeholder "Enter image stream tag (e.g., mystream:latest)")
    cmd="$cmd --to=$image_stream"
fi

# Display the final command
echo "Generated command:"
echo "$cmd"

# Execute or dry run
if gum confirm "Execute this command?"; then
    if eval "$cmd"; then
        gum log --level success "Build created successfully."
    else
        gum log --level error "Failed to create build."
    fi
else
    echo "Dry run output:"
    eval "$cmd --dry-run -o yaml"
fi
