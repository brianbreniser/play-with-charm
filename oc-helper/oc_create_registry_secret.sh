#!/bin/bash

cmd="oc create secret"

# Choose secret type
secret_type=$(gum choose "docker-registry" "generic" "tls" --header="What kind of secret is this?")

# Set common variables
name=$(gum input --placeholder="Enter the name of the secret (e.g., my-registry-secret)")
cmd="$cmd $secret_type $name"

case "$secret_type" in
    "docker-registry")
        server=$(gum input --placeholder="Enter the Docker registry server URL (e.g., https://index.docker.io/v1/)")
        username=$(gum input --placeholder="Enter the Docker registry username (e.g., myusername)")
        password=$(gum input --password --placeholder="Enter the Docker registry password (e.g., mypassword123) (Or Personal access token)")
        email=$(gum input --placeholder="Enter the Docker registry email (optional) (e.g., user@example.com)")

        cmd="$cmd --docker-server=$server --docker-username=$username --docker-password=$password"
        [[ -n "$email" ]] && cmd="$cmd --docker-email=$email"
        ;;
    "generic")
        while true; do
            key=$(gum input --placeholder="Enter key (e.g., API_KEY) (leave blank to finish)")
            [[ -z "$key" ]] && break
            value=$(gum input --placeholder="Enter value for $key (e.g., abcdef123456)")
            cmd="$cmd --from-literal=$key=$value"
        done
        ;;
    "tls")
        cert=$(gum input --placeholder="Enter path to TLS certificate file (e.g., /path/to/tls.crt)")
        key=$(gum input --placeholder="Enter path to TLS key file (e.g., /path/to/tls.key)")
        cmd="$cmd --cert=$cert --key=$key"
        ;;
    *)
        gum log --level error "Invalid secret type selected"
        exit 1
        ;;
esac

# Optional: Append hash
if gum confirm "Append a hash to the secret name?"; then
    cmd="$cmd --append-hash"
fi

# Ask if they want to generate the command, as the password will end up in plain text on the CLI output
if gum confirm "Generate and display the command? (Note: This will show the password in plain text)"; then
    echo "Generated command:"
    echo "$cmd"
fi

# Ask for execution
if gum confirm "Execute this command?"; then
    if eval "$cmd"; then
        gum log --level success "Secret created successfully."
    else
        gum log --level error "Failed to create secret."
        echo "Attempting dry-run with client..."
        eval "$cmd --dry-run=client -o yaml"
    fi
else
    gum log --level info "Command execution cancelled."
fi

# Display YAML output
# Ask for dry-run mode
dry_run=$(gum choose "none" "server" "client" --header="Select dry-run mode:")
cmd="$cmd --dry-run=$dry_run"

# Display YAML output
echo "YAML output:"
eval "$cmd -o yaml"
