#!/bin/bash

cmd="oc create configmap"

# Ask for the name of the configmap
name=$(gum input --placeholder "Enter the name of the config map")

cmd="$cmd $name"

# Choose creation method
method=$(gum choose --header "Select how to create the config map:" "From File" "From Directory" "From Literal" "From Env File")

case $method in
    "From File")
        filePath=$(gum input --placeholder "Enter the full path to the file")
        key=$(gum input --placeholder "Enter the key for the file content (optional)")
        if [ -z "$key" ]; then
            cmd="$cmd --from-file=$filePath"
        else
            cmd="$cmd --from-file=$key=$filePath"
        fi
        ;;
    "From Directory")
        dirPath=$(gum input --placeholder "Enter the path to the directory")
        cmd="$cmd --from-file=$dirPath"
        ;;
    "From Literal")
        literals=()
        while true; do
            literal=$(gum input --placeholder "Enter a literal key=value (leave blank to stop)")
            if [ -z "$literal" ]; then
                break
            fi
            literals+=("$literal")
        done
        for literal in "${literals[@]}"; do
            cmd="$cmd --from-literal=$literal"
        done
        ;;
    "From Env File")
        envFilePath=$(gum input --placeholder "Enter the full path to the env file")
        cmd="$cmd --from-env-file=$envFilePath"
        ;;
esac

# Optional: Append hash
appendHash=$(gum confirm "Append a hash to the config map name?")
if $appendHash; then
    cmd="$cmd --append-hash"
fi

# Display the final command
echo "Generated command:"
echo $cmd

# Ask for execution
execute=$(gum confirm "Execute this command?")
if $execute; then
    eval $cmd
    if [ $? -eq 0 ]; then
        gum log --level success "Config map created successfully."
    else
        gum log --level error "Failed to create config map."
    fi
else
    gum log --level info "Command execution cancelled."
fi

