#!/bin/bash

# Custom function for styled output
gum_style() {
    gum style \
        --border normal \
        --margin "1" \
        --padding "1" \
        --border-foreground 212 \
        "$@"
}

# Check if fzf is installed
if ! command -v fzf &> /dev/null
then
    gum_style "fzf is not installed. It's necessary for this script to function properly.
Please install fzf and try again."
    exit 1
fi

scripts=$(find . -maxdepth 1 -type f -name "oc_*.sh" -not -name "oc-helper.sh")

help() {
    local help_text="This script helps you run the oc command
Usage: bash oc-helper.sh
Available scripts:
"
    for script in $scripts; do
        help_text+="  $(basename "$script")
"
    done
    gum_style "$help_text"
}

# Check if -h flag is passed or no arguments are provided
if [[ "$1" == "-h" ]]; then
    help
    exit 0
fi

# Function to perform fuzzy matching
fuzzy_match() {
    local input=$1
    echo "$scripts" | xargs -n 1 basename | fzf --filter="$input" | xargs -I {} find . -name {}
}

# Get user input
user_input=$(gum input --placeholder "Enter your command")

# Perform fuzzy matching
matched_scripts=$(for script in $scripts; do
    if fuzzy_match "$user_input" | grep -q "$script"; then
        echo "$script"
    fi
done)

# Count matched scripts
match_count=$(echo "$matched_scripts" | wc -l)

if [ "$match_count" -eq 0 ]; then
    gum_style "No matching scripts found."
    help
elif [ "$match_count" -eq 1 ]; then
    script_to_run=$matched_scripts
    gum_style "Running: $script_to_run"
    bash "$script_to_run"
else
    gum_style "Multiple matches found. Please choose:"
    script_to_run=$(echo "$matched_scripts" | xargs -n 1 basename | gum choose)
    gum_style "Running: $script_to_run"
    bash "$script_to_run"
fi
