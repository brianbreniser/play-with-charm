#!/bin/bash

trap 'echo "Script interrupted. Exiting."; exit 1' INT

cmd="oc create role"

# Ask for the name of the role
name=$(gum input --header="Enter the name of the role:")
cmd="$cmd $name"

# Explain verbs
gum style \
  --border normal \
  --margin "1" \
  --padding "1" \
  --border-foreground 212 \
  --width 80 \
  "In OpenShift, verbs define the actions that can be performed on resources. Common verbs include:
  - get: Retrieve a resource
  - list: List all resources of a type
  - watch: Watch for changes to resources
  - create: Create a new resource
  - update: Modify an existing resource
  - patch: Partially modify a resource
  - delete: Remove a resource
  You can specify multiple verbs for a role."

# Gather verbs
verbs=()
valid_verbs=("get" "list" "watch" "create" "update" "patch" "delete")

while true; do
    verb=$(gum input --header="Specify a verb (e.g., get, list, watch) (leave blank to stop):")
    if [ -z "$verb" ]; then
        break
    fi
    if [[ " ${valid_verbs[*]} " =~ ${verb} ]]; then
        verbs+=("$verb")
    else
        gum log --level warn "Invalid verb. Please choose from: ${valid_verbs[*]}"
    fi
done

for verb in "${verbs[@]}"; do
    cmd="$cmd --verb=$verb"
done

# Gather resources
# Explain what a resource is
gum style \
  --border normal \
  --margin "1" \
  --padding "1" \
  --border-foreground 212 \
  --width 80 \
  "In OpenShift, a resource is an object that represents a component of your application or cluster. Examples include pods, services, deployments, and config maps. Some resources may belong to API groups (e.g., 'apps' for ReplicaSets). Specify the resource type, and optionally its API group, that this role will have permissions for."

resources=()
while true; do
    resource=$(gum input --header="Specify a resource (e.g., pods, rs.apps) (leave blank to stop):")
    if [ -z "$resource" ]; then
        break
    fi
    resources+=("$resource")
done

for resource in "${resources[@]}"; do
    cmd="$cmd --resource=$resource"
done

# Explain resource names
gum style \
  --border normal \
  --margin "1" \
  --padding "1" \
  --border-foreground 212 \
  --width 80 \
  "Resource names are specific instances of a resource type. For example, if you're creating a role for pods, you might want to limit it to specific pod names. This is optional and allows for fine-grained control. Leave blank if you want the role to apply to all instances of the specified resource types."

# Gather resource names
resourcenames=()
while true; do
    resourcename=$(gum input --header="Specify a resource name (leave blank to stop):")
    if [ -z "$resourcename" ]; then
        break
    fi
    resourcenames+=("$resourcename")
done

for resourcename in "${resourcenames[@]}"; do
    cmd="$cmd --resource-name=$resourcename"
done

# Explain dry-run mode
gum style \
  --border normal \
  --margin "1" \
  --padding "1" \
  --border-foreground 212 \
  --width 80 \
  "Dry-run mode allows you to preview the effects of a command without actually making changes:
  - 'none': The command will be executed normally.
  - 'client': Simulates the command client-side without sending to the server.
  - 'server': Sends the request to the server but doesn't persist changes.
  This is useful for validating your role configuration before applying it."

# Optional: Dry-run mode
dry_run=$(gum choose "none" "server" "client" --header="Select dry-run mode:")
cmd="$cmd --dry-run=$dry_run"

# Display the final command
echo "Generated command:"
echo $cmd

# Ask for execution
execute=$(gum confirm "Execute this command?")
if $execute; then
    eval $cmd
    if [ $? -eq 0 ]; then
        gum log --level success "Role created successfully."
    else
        gum log --level error "Failed to create role."
    fi
else
    gum log --level info "Command execution cancelled."
fi
