#!/bin/bash

cmd="oc new-app"

# Choose build strategy
strategy=$(gum choose --header="Choose the build strategy:" "docker" "source" "pipeline")

case $strategy in
    "pipeline")
        gum log --level error "Pipeline strategy is deprecated. Consider using Jenkinsfiles directly on Jenkins or OpenShift Pipelines."
        exit 1
        ;;
    *)
        cmd="$cmd --strategy=$strategy"
        ;;
esac

# Check if binary build is needed
if gum confirm "Is this a binary build?"; then
    cmd="$cmd --binary"
fi

# Get name of the app if needed
name=$(gum input --placeholder "Enter the name of the application (optional)")
if [ -n "$name" ]; then
    cmd="$cmd --name=$name"
fi

# Source code URL or path
if [ "$strategy" != "binary" ]; then
    source=$(gum input --placeholder "Enter the URL or Path to the source code (leave blank for image-based app)")
    if [ -n "$source" ]; then
        cmd="$cmd $source"
    fi
fi

# Environment Variables
envlist=()
while true; do
    env=$(gum input --placeholder "Specify an environment variable (KEY=value, leave blank to stop)")
    if [ -z "$env" ]; then
        break
    fi
    envlist+=("$env")
done

for env in "${envlist[@]}"; do
    cmd="$cmd -e $env"
done

# Display the final command
echo "Generated command:"
echo "$cmd"

# Ask for execution
if gum confirm "Execute this command?"; then
if eval "$cmd"; then
    gum log --level success "Application creation initiated successfully."
else
    gum log --level error "Application creation failed."
fi
else
    gum log --level info "Command execution cancelled."
fi
